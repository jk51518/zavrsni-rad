package main

import (
	"encoding/json"
	"github.com/gin-gonic/gin"
	"github.com/gin-gonic/gin/binding"
	"io/ioutil"
	"log"
	"math/rand"
	"net/http"
	"os"
	"strconv"
)

type GenerationRequestBody struct {
	Count int `json:"count"`
}

type PrimeCheckerServiceResponse struct {
	IsPrime bool `json:"isPrime"`
}

var primeCheckerServiceURL string

func sendPrimeCheckerRequest() {
	req, err := http.NewRequest("GET", primeCheckerServiceURL, nil)
	if err != nil {
		log.Print(err)
		return
	}

	number := rand.Int()
	q := req.URL.Query()
	q.Add("number", strconv.Itoa(number))
	req.URL.RawQuery = q.Encode()
	rawRes, err := http.DefaultClient.Do(req)
	if err != nil {
		log.Print(err)
		return
	}
	defer rawRes.Body.Close()
	rawBody, err := ioutil.ReadAll(rawRes.Body)
	if err != nil {
		log.Print(err)
		return
	}

	var body PrimeCheckerServiceResponse
	err = json.Unmarshal(rawBody, &body)
	if err != nil {
		log.Print(err)
		return
	}
	var isPrime string
	if body.IsPrime {
		isPrime = "YES"
	} else {
		isPrime = "NO"
	}

	log.Printf("%d: %s\n", number, isPrime)
}

func main() {
	router := gin.Default()
	primeCheckerServiceURL = os.Getenv("PRIME_CHECKER_SERVICE_URL")
	if primeCheckerServiceURL == "" {
		panic("Prime checker service URL not set")
	}

	router.POST("/generate", func(c *gin.Context) {
		var body GenerationRequestBody
		err := c.ShouldBindBodyWith(&body, binding.JSON)
		if err != nil {
			log.Print(err.Error())
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}
		if body.Count <= 0 {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		for i := 0; i < body.Count; i++ {
			sendPrimeCheckerRequest()
		}

		c.Status(http.StatusOK)
	})

	err := router.Run()
	if err != nil {
		panic(err)
	}
}
