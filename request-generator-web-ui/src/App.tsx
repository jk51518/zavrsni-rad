import React, { useCallback, useEffect, useState } from 'react';
import { Button,  InputNumber,  message} from 'antd';
import 'antd/dist/antd.css';
import classNames from './App.module.css';
import clsx from 'clsx';


const withTimeout = <T extends unknown>(promise: Promise<T>, timeoutMS: number,): Promise<T> => {
    return new Promise<T>((resolve, reject) => {
      const timerID = setTimeout(reject, timeoutMS);
      promise.then((value) => resolve(value))
            .catch((err) => resolve(err))
            .finally(() => clearTimeout(timerID))
    });
}

function App() {
  const [requestInProgress, setRequestInProgress] = useState(false);
  const [requestCount, setRequestCount] = useState(1);
  const sendRequest = useCallback(() => {
    setRequestInProgress(true);
    withTimeout(fetch('http://localhost:31695/generate', {
      method: 'POST',
      mode: 'no-cors',
      body: JSON.stringify({count: requestCount}),
    }), 1000)
    .then((res) => {
      message.success("Request sent successfully");
      setRequestCount(1);
    })
    .catch((e) => {
      message.error("Request not sent");
    })
    .finally(() => {
      setRequestInProgress(false);
    })
  }, [setRequestInProgress, requestCount])

  return (
    <div className={clsx(classNames['full-screen'], classNames['center-children'], classNames['root-wrapper'])}>
      <div className={classNames['title-wrapper']}>
        <h1>Request Generator</h1>
      </div>
      <div className={clsx(classNames['request-form'], classNames['center-children'])}>
        <div>
          <span>Number of requests:</span>
          <InputNumber 
            disabled={requestInProgress}
            value={requestCount} 
            min={1}
            max={1000000}
            onChange={setRequestCount}
            style={{
              marginLeft: '10px'
            }}
          />
        </div>
        <Button 
          type="dashed" 
          className={classNames['button']}
          onClick={sendRequest}
          loading={requestInProgress}
        > 
          Send requests! 
        </Button>
      </div>
    </div>
  );
}

export default App;
