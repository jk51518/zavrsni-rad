package main

import (
	"github.com/gin-gonic/gin"
	"net/http"
	"strconv"
)

type PrimeCheckerServiceResponse struct {
	IsPrime bool `json:"isPrime"`
}

var primeCheckerServiceURL string

func isPrime(n int) bool {
	for i := 2; i < n; i++ {
		if n%i == 0 {
			return false
		}
	}
	return true
}

func main() {
	router := gin.Default()

	router.GET("/", func(c *gin.Context) {
		numberParam := c.Request.URL.Query().Get("number")
		if numberParam == "" {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		number, err := strconv.Atoi(numberParam)
		if err != nil {
			c.AbortWithStatus(http.StatusBadRequest)
			return
		}

		c.JSON(http.StatusOK, gin.H{
			"isPrime": isPrime(number),
		})
	})

	err := router.Run()
	if err != nil {
		panic(err)
	}
}
